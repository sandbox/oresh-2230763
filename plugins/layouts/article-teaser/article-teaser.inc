<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('News Teaser'),
  'category' => t('Bootstrap Columns'),
  'icon' => 'article-teaser.png',
  'theme' => 'panels_article_teaser',
  'css' => 'article-teaser.css',
  'settings' => array('use_container' => NULL, 'panel_class' => NULL),
  'settings form' => 'bootstrap_layout_settings_form',
  'regions' => array(
    'one' => t('Left'),
    'two' => t('Right'),
  ),
);
