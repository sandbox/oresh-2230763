<?php

/**
 * @file
 * Implementation of hook_panels_layouts().
 */

$circle_path = drupal_get_path('theme', 'circle');
include_once './' . $circle_path . '/plugins/layouts/bootstrap-settings.inc';

$plugin = array(
  'title' => t('Frontpage'),
  'category' => t('Bootstrap Columns'),
  'icon' => 'frontpage.png',
  'theme' => 'panels_frontpage',
  'css' => 'frontpage.css',
  'settings' => array('use_container' => NULL, 'panel_class' => NULL),
  'settings form' => 'bootstrap_layout_settings_form',
  'regions' => array(
    'one' => t('Top'),
    'two' => t('Left Above'),
    'three' => t('Right Above'),
    'four' => t('Middle'),
    'five' => t('Footer'),
    'six' => t('Footer Second')
  ),
);
