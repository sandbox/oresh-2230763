<?php
/**
 * @file
 * Write your theme logic here.
 */

function people_css_alter(&$css) {
  // Exclude all the unused core and modules css.
    $exclude = array(
      'profiles/pp/modules/contrib/panels/css/panels.css' => FALSE,
    );
    $css = array_diff_key($css, $exclude);
 }