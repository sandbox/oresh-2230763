<?php
/**
 * @file
 *
 * Theme implementation to display the header block on a Drupal page.
 *
 * This utilizes the following variables thata re normally found in
 * page.tpl.php:
 * - $logo
 * - $front_page
 * - $site_name
 * - $front_page
 * - $site_slogan
 * - $search_box
 *
 * Additional items can be added via theme_preprocess_pane_header(). See
 * template_preprocess_pane_header() for examples.
 */
 ?>
<div id="header">
  <div class="section clearfix">
    <div id="logo-title">


      <div id="name-and-slogan">

        <?php if (!empty($site_slogan)): ?>
          <div id="site-slogan"><?php print $site_slogan; ?></div>
        <?php endif; ?>
      </div> <!-- /name-and-slogan -->
    </div> <!-- /logo-title -->

    <?php if (!empty($search_box)): ?>
      <div id="search-box"><?php print $search_box; ?></div>
    <?php endif; ?>
  </div> <!-- /section -->
</div> <!-- /header -->
<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#page-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <?php if (!empty($logo)): ?>
        <a href="<?php print $front_page; ?>" class="navbar-brand" title="<?php print t('Home'); ?>" rel="home" id="logo">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
      <?php endif; ?>
      <?php if ($site_name): ?>
        <a href="<?php print $front_page; ?>" class="site-name-wrapper" title="<?php print t('Home'); ?>" rel="home">
          <span><?php print $site_name; ?></span>
        </a>
      <?php endif; ?>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="page-navbar-collapse">
      <?php
        print theme('links',
          array('links' => menu_navigation_links('main-menu'),
            'attributes' => array(
              'class'=> array(
                'nav',
                'navbar-nav',
              )
            )
          )
        ); 
      ?>
      
      <?php 
        print theme('links',
          array('links' => menu_navigation_links('user-menu'),
            'attributes' => array(
              'class'=> array(
                'nav',
                'navbar-nav',
                'navbar-right'
              )
            )
          )
        ); 
      ?>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>